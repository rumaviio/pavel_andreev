<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# Auth
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    # Home
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

    # Profile Management
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'UsersController@edit']);
    Route::patch('profile/update', ['as' => 'profile.update', 'uses' => 'UsersController@update']);

    # Photos Management
    Route::resource('photos', 'PhotosController')->except([
        'edit', 'update', 'destroy'
    ]);
    Route::post('photos/comment', ['as' => 'photos.comment.store', 'uses' => 'PhotoCommentsController@store']);

    Route::get('user/{user}/photos', ['as' => 'user.photos', 'uses' => 'UsersController@photos']);

    # Catalogs Management
    Route::resource('catalogs', 'CatalogsController')->except([
        'edit', 'update', 'destroy'
    ]);
});
