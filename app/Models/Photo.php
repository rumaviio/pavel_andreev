<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Photo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'photos';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'image', 'user_id'];

    /**
     * Get photo catalogs.
     */
    public function catalogs(): BelongsToMany
    {
        return $this->belongsToMany(Catalog::class);
    }

    /**
     * Get photo comments.
     */
    public function comments(): HasMany
    {
        return $this->hasMany(PhotoComment::class);
    }

    /**
     * Get the user who owns the photo.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get image filename
     *
     * @return string.
     */
    public function getImageFilenameAttribute(): string
    {
        return $this->id . '-' . $this->image;
    }
}
