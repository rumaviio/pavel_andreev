<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Catalog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'catalogs';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_id'];

    public static function visibleColumns()
    {
        return [
            '#' => 'catalogs.id',
            'catalogs/general.name' => 'catalogs.name'
        ];
    }

    /**
     * Get photo catalogs.
     */
    public function photos(): BelongsToMany
    {
        return $this->belongsToMany(Photo::class);
    }

    /**
     * Get the user who owns the catalog.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
