<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoCommentRequest;
use App\Models\PhotoComment;
use Illuminate\Http\RedirectResponse;

class PhotoCommentsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param PhotoCommentRequest $request
     *
     * @return RedirectResponse
     */
    public function store(PhotoCommentRequest $request): RedirectResponse
    {
        $commentData = array_merge($request->all(), ['user_id' => auth()->id()]);

        PhotoComment::create($commentData);

        return redirect()
            ->back()
            ->with('success', trans('message.success.create'));
    }
}
