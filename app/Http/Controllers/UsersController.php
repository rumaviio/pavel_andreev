<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class UsersController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return View
     */
    public function edit()
    {
        return view('users.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     *
     * @return RedirectResponse|Redirector
     */
    public function update(UserRequest $request)
    {
        auth()->user()->update($request->all());

        return redirect()->route("home")->with('success', trans('message.success.update'));
    }

    /**
     * Get user photos
     *
     * @param User $user
     *
     * @return View
     */
    public function photos(User $user): View
    {
        $user->load('photos');

        return view('photos.index')
            ->with('photos', $user->photos);
    }
}
