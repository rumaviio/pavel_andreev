<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoRequest;
use App\Models\Catalog;
use App\Models\Photo;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $photos = Photo::with('user')->orderBy('id', 'DESC')->get();

        return view('photos.index')
            ->with('photos', $photos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $catalogs = Catalog::where('user_id', auth()->id())->pluck('name', 'id');

        return view('photos.create')
            ->with('catalogs', $catalogs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PhotoRequest $request
     *
     * @return RedirectResponse
     */
    public function store(PhotoRequest $request): RedirectResponse
    {
        $photoData = [
            'name' => $request->get('name'),
            'image' => $request->file('image')->getClientOriginalName(),
            'user_id' => auth()->id()
        ];

        $photo = Photo::create($photoData);

        $photo->catalogs()->sync($request->get('catalogs'));

        $request->file('image')->storeAs('photos', $photo->imageFilename, 'public');

        return redirect()
            ->route("photos.index")
            ->with('success', trans('message.success.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param Photo $photo
     *
     * @return View
     */
    public function show(Photo $photo): View
    {
        $photo->load('catalogs', 'comments');

        return view('photos.show')
            ->with('photo', $photo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Photo $photo
     *
     * @return View
     */
    public function edit(Photo $photo): View
    {
        $catalogs = Catalog::where('user_id', auth()->id())->pluck('name', 'id');

        return view('photos.edit')
            ->with('photo', $photo)
            ->with('catalogs', $catalogs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Photo $photo
     * @param PhotoRequest $request
     *
     * @return RedirectResponse
     */
    public function update(Photo $photo, PhotoRequest $request): RedirectResponse
    {
        $photo->update($request->all());

        return redirect()
            ->route("photos.index")
            ->with('success', trans('message.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Photo $photo
     *
     * @return RedirectResponse
     */
    public function destroy(Photo $photo): RedirectResponse
    {
        $photo->delete();

        return redirect()
            ->route("photos.index")
            ->with('success', trans('message.success.delete'));
    }
}
