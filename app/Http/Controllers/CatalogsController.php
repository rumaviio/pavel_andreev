<?php

namespace App\Http\Controllers;

use App\Http\Requests\CatalogRequest;
use App\Models\Catalog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Yajra\DataTables\DataTables;

class CatalogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CatalogRequest $request
     * @param DataTables $dataTables
     *
     * @return JsonResponse | View
     * @throws \Exception
     */
    public function index(CatalogRequest $request, DataTables $dataTables)
    {
        if ($request->wantsJson()) {
            return $this->data($dataTables);
        }

        return view('catalogs.index');
    }

    /**
     * Get data for dataTables
     *
     * @param DataTables $dataTables
     *
     * @return JsonResponse
     * @throws \Exception
     */
    private function data(DataTables $dataTables)
    {
        $catalogs = Catalog::select(array_values(Catalog::visibleColumns()))
            ->where('user_id', auth()->id());

        return $dataTables->eloquent($catalogs)
            ->addColumn('action', function (Catalog $catalog) {
                return view('partials.fullActions', [
                    'module' => 'catalogs',
                    'item' => $catalog,
                    'noEdit' => true,
                    'noDestroy' => true,
                ]);
            })
            ->rawColumns([count(Catalog::visibleColumns())])
            ->make(false);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('catalogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CatalogRequest $request
     *
     * @return RedirectResponse
     */
    public function store(CatalogRequest $request): RedirectResponse
    {
        $catalogData = array_merge($request->all(), ['user_id' => auth()->id()]);

        Catalog::create($catalogData);

        return redirect()
            ->route("catalogs.index")
            ->with('success', trans('message.success.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param Catalog $catalog
     *
     * @return View
     */
    public function show(Catalog $catalog): View
    {
        $catalog->load('photos');

        return view('catalogs.show')
            ->with('catalog', $catalog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Catalog $catalog
     *
     * @return View
     */
    public function edit(Catalog $catalog): View
    {
        return view('catalogs.edit')
            ->with('catalog', $catalog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Catalog $catalog
     * @param CatalogRequest $request
     *
     * @return RedirectResponse
     */
    public function update(Catalog $catalog, CatalogRequest $request): RedirectResponse
    {
        $catalog->update($request->all());

        return redirect()
            ->route("catalogs.index")
            ->with('success', trans('message.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Catalog $catalog
     *
     * @return RedirectResponse
     */
    public function destroy(Catalog $catalog)
    {
        $catalog->delete();

        return redirect()
            ->route("catalogs.index")
            ->with('success', trans('message.success.delete'));
    }
}
