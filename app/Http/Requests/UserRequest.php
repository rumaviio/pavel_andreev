<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required|between:2,255',
            'last_name' => 'required|between:2,255',
            'password' => 'required|between:6,32',
            'password_confirmation' => 'required|same:password',
        ];

        switch ($this->method()) {
            case 'POST':
                return array_merge($rules, [
                    'email' => 'required|email|max:255|unique:users',
                ]);
            case 'PUT':
            case 'PATCH':
                return array_merge($rules, [
                    'email' => 'required|email|max:255|unique:users,email,' . auth()->id(),
                    'password' => 'nullable|between:6,32',
                    'password_confirmation' => 'nullable|same:password',
                ]);
            default:
                return [];
        }
    }
}
