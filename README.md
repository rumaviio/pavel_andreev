## Technologies used

1. PHP 7.4 (http://php.net)
2. MySQL 8.3 (http://mysql.com)
3. Composer
4. Laravel 8 (http://laravel.com)
5. HTML Collective (https://laravelcollective.com/docs/6.0/html)

## Requirements

1. PHP 7.3 and above
2. PHP OpenSSL extension
3. PHP PDO extension
4. PHP Mbstring extension
5. PHP Tokenizer extension
6. PHP XML extension
7. PHP Ctype extension
8. PHP JSON extension

## Installation

### Install Composer
Tool is based on Laravel, which utilizes [Composer](http://getcomposer.org) to manage its dependencies. 
First, download a copy of the `composer.phar`.
Once you have the PHAR archive, you can either keep it in your local project directory or move to `usr/local/bin` to use it globally on your system. 
On Windows, you can use the Composer [Windows installer](https://getcomposer.org/Composer-Setup.exe).

### Install Project
1. Download or checkout the latest copy from here (https://gitlab.com/rumaviio/pavel_andreev).
2. Install the required dependencies with composer. `composer install`.
3. If you have not renamed the `.env.example` file to `.env`, you should do that now.
4. Generate a new application key: `php artisan key:generate`.
5. Set database in `.env` file.
6. Bringing machine up with `php artisan serve` command.
7. To run all of your outstanding migrations, execute the `migrate` Artisan command: `php artisan migrate`.

## Author Information

The tool originally started and is maintained by [Rumen Yanev](https://gitlab.com/rumaviio).
