{!! Html::script('assets/select2-4.0.3/js/select2.min.js') !!}

<script>
    jQuery(function($) {
        var fieldClass = 'defaultSelect2';
        {{-- Set custom filed class --}}
        @if (isset($fieldClass))
            fieldClass = '{{ $fieldClass }}';
        @endif

        $('.' + fieldClass).select2({
            language: "{{ App::getLocale() }}",
            placeholder: "{{ trans('general.pleaseChoose') }}",
            width: "100%",
            allowClear: true,
        });
    });
</script>
