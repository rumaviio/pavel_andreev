<aside>
    <div id="sidebar" class="nav-collapse">
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="{{ route('home') }}" {!! (request()->routeIs('home') ? 'class="active"' : '') !!}>
                        <i class="fa fa-home"></i>
                        {{ trans('menu.home') }}
                    </a>
                </li>

                <li>
                    <a href="{{ route('photos.index') }}" {!! (request()->is('photos') || request()->is('photos/*') ? 'class="active"' : '') !!}>
                        <i class="fa fa-photo"></i>
                        {{ trans('menu.photos') }}
                    </a>
                </li>

                <li>
                    <a href="{{ route('catalogs.index') }}" {!! (request()->is('catalogs') || request()->is('catalogs/*') ? 'class="active"' : '') !!}>
                        <i class="fa fa-book"></i>
                        {{ trans('menu.catalogs') }}
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>
