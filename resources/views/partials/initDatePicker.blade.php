{!! Html::script('assets/js/bootstrap-datepicker.min.js') !!}

<script>
    jQuery(function($) {
        var fieldClass = 'defaultDatePicker';
        {{-- Set custom filed class --}}
        @if (isset($fieldClass))
            fieldClass = '{{ $fieldClass }}';
        @endif

        $('.' + fieldClass).datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            language: "{{ App::getLocale() }}",
            weekStart: 1,
            todayHighlight: true,
            todayBtn: "linked",
        });
    });
</script>
