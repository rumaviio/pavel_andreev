<div aria-hidden="true" role="dialog" tabindex="-1" id="destroyModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">{{ trans('modal.destroy.title') }}</h4>
            </div>

            <div class="modal-body form-horizontal">
                <div class="col-sm-12">
                    {{ trans('modal.destroy.body') }}
                </div>

                <div class="col-sm-12">
                    {!! Form::open(['id' => 'destroy-form', 'method' => 'DELETE']) !!}
                        {!! Form::submit(trans('modal.destroy.confirm'), ['class' => 'btn btn-danger col-sm-4']) !!}
                    {!! Form::close() !!}
                    <button data-dismiss="modal" type="button" class="btn btn-default col-sm-4 col-sm-offset-4">
                        {{ trans('modal.destroy.cancel') }}
                    </button>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
