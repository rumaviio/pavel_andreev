<meta charset="UTF-8">
<title>
    Task
</title>

<link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

{!! Html::style('assets/css/bootstrap.min.css') !!}
{!! Html::style('assets/css/bootstrap-reset.css') !!}
{!! Html::style('assets/font-awesome-4.7.0/css/font-awesome.min.css') !!}
{!! Html::style('assets/select2-4.0.3/css/select2.min.css') !!}
{!! Html::style('assets/datatables/1.10.15/css/jquery.dataTables.min.css') !!}
{!! Html::style('assets/css/datepicker.css') !!}
{!! Html::style('assets/css/style.css') !!}
{!! Html::style('assets/css/style-responsive.css') !!}
{!! Html::style('assets/css/custom.css') !!}
