@if (! isset($noShow))
    <a href="{{ route($module . '.show', [Str::singular($module) => $item]) }}"
       class="btn btn-sm btn-info"
       title="{{ trans('button.view') }}">
        <i class="fa fa-eye"></i>
    </a>
@endif

@if (! isset($noEdit))
    <a href="{{ route($module . '.edit', [Str::singular($module) => $item]) }}"
       class="btn btn-sm btn-warning"
       title="{{ trans('button.edit') }}">
        <i class="fa fa-edit"></i>
    </a>
@endif

@if (! isset($noDestroy))
    <a class="btn btn-sm btn-danger"
       href="#destroyModal" data-toggle="modal"
       title="{{ trans('button.delete') }}"
       data-url="{{ route($module . '.destroy', [Str::singular($module) => $item]) }}">
        <i class="fa fa-trash-o"></i>
    </a>
@endif
