<script>
    jQuery(function($) {
        let id = 'defaultDataTable';
        {{-- Set custom table id --}}
        @if (isset($tableId))
            id = '{{ $tableId }}';
        @endif

        let table = $('#' + id).DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: "{{ request()->url() }}",
                    data: function (data) {
                        data.date = $('#date').val();
                    }
                },
                "order": [
                    @if (isset($orderColumns))
                        @foreach ($orderColumns as $orderColumn => $orderType)
                            [ {{ $orderColumn }}, "{{ $orderType }}" ],
                        @endforeach
                    @endif
                ],
                "aoColumnDefs": [
                    @if (isset($unsearchableColumns))
                        { "bSearchable": false, "aTargets": {!! json_encode((array) $unsearchableColumns) !!} }
                    @endif
                    @if (isset($unsortableColumns))
                        @if (isset($unsearchableColumns)) , @endif
                        { "bSortable": false, "aTargets": {!! json_encode((array) $unsortableColumns) !!} }
                    @endif
                    @if (isset($columnDefs))
                        @foreach ($columnDefs as $class => $targets)
                            { "className": "{{ $class }}", "targets": {!! json_encode((array) $targets) !!} }
                        @endforeach
                    @endif
                ],
                "stateSave": true,
            });

        let param = getParameterByName('search');

        if (param) {
            table.search(param).draw();
        }

        $('div.dataTables_filter input').attr('name', 'dataTablesSearch');

        $('#search-form').on('submit', function(e) {
            e.preventDefault();
            table.draw();
        });
    });
</script>
