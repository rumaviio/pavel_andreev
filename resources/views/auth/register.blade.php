<!DOCTYPE html>
<html>
    <head>
        @include('partials.head')
    </head>

    <body class="login-body">
        <div class="container">
            @include('notifications')

            {!! Form::open(['method' => 'POST', 'url' => route('register'), 'class' => 'form-signin']) !!}
                <h2 class="form-signin-heading">{{ trans('auth/form.registration') }}</h2>
                <div class="login-wrap">
                    {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => trans('auth/form.firstName')]) !!}

                    {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => trans('auth/form.lastName')]) !!}

                    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('auth/form.email')]) !!}

                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('auth/form.password')]) !!}

                    {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('auth/form.passwordConfirmation')]) !!}

                    {!! Form::submit(trans('button.submit'), ['class' => 'btn btn-lg btn-login btn-block']) !!}

                    <div class="registration">
                        {{ trans('auth/form.alreadyRegistered') }}
                        <a class="" href="{{ route('login') }}">
                            {{ trans('auth/form.login') }}
                        </a>
                    </div>

                </div>
            {!! Form::close() !!}

            @include('partials.footer')
        </div>
    </body>
</html>
