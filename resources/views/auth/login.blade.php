<!DOCTYPE html>
<html>
    <head>
        @include('partials.head')
    </head>

    <body class="login-body">
        <div class="container">
            @include('notifications')

            {!! Form::open(['method' => 'POST', 'url' => route('login'), 'class' => 'form-signin']) !!}
            <h2 class="form-signin-heading">
                {{ trans('auth/form.login') }}
            </h2>
            <div class="login-wrap">
                <div class="user-login-info">
                    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('auth/form.email'), 'autofocus' => 'autofocus']) !!}

                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('auth/form.password')]) !!}
                </div>

                {!! Form::submit(trans('auth/form.login'), ['class' => 'btn btn-lg btn-login btn-block']) !!}

                <div class="registration">
                    {{ trans('auth/form.registration') }}
                    <a class="" href="{{ route('register') }}">
                        {{ trans('auth/form.createAccount') }}
                    </a>
                </div>
            </div>
            {!! Form::close() !!}

            @include('partials.footer')
        </div>
    </body>
</html>
