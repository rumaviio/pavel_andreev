@extends('layouts.default')

@section('content')
    <div class="row-fluid">
        <section class="panel">
            <header class="panel-heading">
                <h4 class="pull-left">
                    {{ trans('button.edit') }}
                </h4>
                <div class="clearfix"></div>
            </header>

            <div class="panel-body">
                {!! Form::model(auth()->user(), ['method' => 'PATCH', 'url' => route('profile.update')]) !!}
                    <div class="form-group {{ $errors->first('first_name', 'has-error') }}">
                        {!! Form::label('first_name', trans('users/general.firstName'), ['class' => 'control-label text-right col-sm-3']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group {{ $errors->first('last_name', 'has-error') }}">
                        {!! Form::label('last_name', trans('users/general.lastName'), ['class' => 'control-label text-right col-sm-3']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                        {!! Form::label('email', trans('users/general.email'), ['class' => 'control-label text-right col-sm-3']) !!}
                        <div class="col-sm-9">
                            {!! Form::email('email', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group {{ $errors->first('password', 'has-error') }}">
                        {!! Form::label('password', trans('users/general.password'), ['class' => 'control-label text-right col-sm-3']) !!}
                        <div class="col-sm-9">
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group {{ $errors->first('password_confirmation', 'has-error') }}">
                        {!! Form::label('password_confirmation', trans('users/general.passwordConfirmation'), ['class' => 'control-label text-right col-sm-3']) !!}
                        <div class="col-sm-9">
                            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            {!! Form::submit(trans('button.edit'), ['class' => 'col-sm-12 btn btn-warning']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection