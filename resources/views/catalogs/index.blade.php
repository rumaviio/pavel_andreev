@extends('layouts.default')

@section('content')
    <div class="row-fluid">
        <section class="panel">
            <header class="panel-heading">
                <h4 class="pull-left">{{ trans('menu.catalogs') }}</h4>
                <a href="{{ route('catalogs.create') }}" class="pull-right btn btn-success">
                    {{ trans('button.create') }}
                </a>
                <div class="clearfix"></div>
            </header>

            <div class="panel-body">
                <table class="table table-hover" id="defaultDataTable">
                    <thead>
                        <tr>
                            @foreach (\App\Models\Catalog::visibleColumns() as $visibleColumnKey => $visibleColumn)
                                <th>{{ trans($visibleColumnKey) }}</th>
                            @endforeach
                            <th>{{ trans('general.actions') }}</th>
                        </tr>
                    </thead>

                    <tbody>
                    </tbody>
                </table>
            </div>
        </section>
    </div>

    @include('partials.destroyModal')
@endsection

@section('footer_scripts')
    @include('partials.initDataTables')
@endsection
