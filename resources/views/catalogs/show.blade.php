@extends('layouts.default')

@section('content')
    <div class="row-fluid">
        <section class="panel">
            <header class="panel-heading">
                <h4 class="pull-left">
                    {{ trans('menu.catalogs') }}
                </h4>
                <a class="pull-right btn btn-primary" href="{{ route('catalogs.index') }}">
                    {{ trans('menu.catalogs') }}
                </a>

                <div class="clearfix"></div>
            </header>

            <div class="panel-body">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>{{ trans('general.id') }}</th>
                            <td>{{ $catalog->id }}</td>
                        </tr>
                        <tr>
                            <th>{{ trans('catalogs/general.name') }}</th>
                            <td>{{ $catalog->name }} </td>
                        </tr>
                        <tr>
                            <th colspan="2">{{ trans('catalogs/general.photos') }}</th>
                        </tr>
                    </tbody>
                </table>

                <div class="container">
                    <div class="row col-md-11">
                        @foreach ($catalog->photos as $photo)
                            <div class="col-md-2" >
                                <div class="thumbnail" style="max-height: 100px">
                                    <img src="{{ asset('storage/photos/' . $photo->imageFilename) }}" alt="{{ $photo->name }}" class="preview" />
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
