@extends('layouts.default')

@section('content')
    <div class="row-fluid">
        <section class="panel">
            <header class="panel-heading">
                <h4 class="pull-left">
                    {{ trans('button.edit') }}
                </h4>
                <a class="pull-right btn btn-primary" href="{{ route('catalogs.index') }}">
                    {{ trans('button.back') }}
                </a>
                <div class="clearfix"></div>
            </header>

            <div class="panel-body">
                {!! Form::model($catalog, ['method' => 'PATCH', 'url' => route('catalogs.update', $catalog)]) !!}
                    @include('catalogs.form', ['update' => true])
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
