<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', trans('catalogs/general.name'), ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {{ Form::text('name', null, ['class' => 'form-control md-input']) }}
    </div>
    <div class="clearfix"></div>
</div>

<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
        @if (isset($update))
            {!! Form::submit(trans('button.edit'), ['class' => 'col-sm-12 btn btn-warning']) !!}
        @else
            {!! Form::submit(trans('button.save'), ['class' => 'col-sm-12 btn btn-success']) !!}
        @endif
    </div>
</div>
