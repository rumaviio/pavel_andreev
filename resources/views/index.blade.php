@extends('layouts.default')

@section('content')
    <div class="row-fluid">
        <section class="panel">
            <header class="panel-heading">
                <h4 class="pull-left">{{ trans('menu.home') }}</h4>
                <div class="clearfix"></div>
            </header>
        </section>
    </div>

    @include('partials.destroyModal')
@endsection
