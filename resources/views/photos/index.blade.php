@extends('layouts.default')

@section('content')
    <div class="row-fluid">
        <section class="panel">
            <header class="panel-heading">
                <h4 class="pull-left">{{ trans('menu.photos') }}</h4>
                <a href="{{ route('photos.create') }}" class="pull-right btn btn-success">
                    {{ trans('button.create') }}
                </a>
                <div class="clearfix"></div>
            </header>

            <div class="panel-body">
                <table class="table table-hover" id="defaultDataTable">
                    <thead>
                        <tr>
                            <th>{{ trans('photos/general.image') }}</th>
                            <th>{{ trans('photos/general.author') }}</th>
                            <th>{{ trans('general.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($photos as $photo)
                            <tr>
                                <td>
                                    <img src="{{ asset('storage/photos/' . $photo->imageFilename) }}" alt="{{ $photo->name }}" class="preview" style="max-width:100px; max-height:100px;" />
                                </td>
                                <td>
                                    <a href="{{ route('user.photos', $photo->user) }}">
                                        {{ $photo->user->name }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('photos.show', ['photo' => $photo]) }}"
                                       class="btn btn-sm btn-info"
                                       title="{{ trans('button.view') }}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>

    @include('partials.destroyModal')
@endsection
