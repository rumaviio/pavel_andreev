@section('header_styles')
    <link href="{{ asset('assets/select2-4.0.3/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
@stop

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', trans('photos/general.name'), ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {{ Form::text('name', null, ['class' => 'form-control md-input']) }}
    </div>
    <div class="clearfix"></div>
</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('photos/general.image'), ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {{ Form::file('image', ['class' => 'form-control md-input']) }}
    </div>
    <div class="clearfix"></div>
</div>

<div class="form-group {{ $errors->has('catalogs') ? 'has-error' : ''}}">
    {!! Form::label('catalogs', trans('photos/general.catalogs'), ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::select('catalogs[]', $catalogs ?? [], null, ['class' => 'form-control defaultSelect2', 'multiple' => 'multiple']) !!}
    </div>
    <div class="clearfix"></div>
</div>

<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
        @if (isset($update))
            {!! Form::submit(trans('button.edit'), ['class' => 'col-sm-12 btn btn-warning']) !!}
        @else
            {!! Form::submit(trans('button.save'), ['class' => 'col-sm-12 btn btn-success']) !!}
        @endif
    </div>
</div>

@section('footer_scripts')
    @parent

    @include('partials.initSelect2')
@endsection
