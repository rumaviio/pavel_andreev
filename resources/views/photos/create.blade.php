@extends('layouts.default')

@section('content')
    <div class="row-fluid">
        <section class="panel">
            <header class="panel-heading">
                <h4 class="pull-left">
                    {{ trans('button.create') }}
                </h4>
                <a class="pull-right btn btn-primary" href="{{ route('photos.index') }}">
                    {{ trans('button.back') }}
                </a>
                <div class="clearfix"></div>
            </header>

            <div class="panel-body">
                {!! Form::open(['method' => 'POST', 'url' => route('photos.store'), 'files' => true]) !!}
                    @include('photos.form')
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
