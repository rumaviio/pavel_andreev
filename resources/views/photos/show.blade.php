@extends('layouts.default')

@section('content')
    <div class="row-fluid">
        <section class="panel">
            <header class="panel-heading">
                <h4 class="pull-left">
                    {{ trans('menu.photos') }}
                </h4>
                <a class="pull-right btn btn-primary" href="{{ route('photos.index') }}">
                    {{ trans('menu.photos') }}
                </a>

                <div class="clearfix"></div>
            </header>

            <div class="panel-body">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>{{ trans('photos/general.name') }}</th>
                            <td colspan="2">{{ $photo->name }}</td>
                        </tr>
                        <tr>
                            <th>{{ trans('photos/general.image') }}</th>
                            <td colspan="2">
                                <img src="{{ asset('storage/photos/' . $photo->imageFilename) }}" alt="{{ $photo->name }}" class="preview" style="max-width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <th>{{ trans('photos/general.catalogs') }}</th>
                            <td colspan="2">
                                @foreach ($photo->catalogs as $catalog)
                                    <span class="label label-default">{{ $catalog->name }}</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th colspan="3">{{ trans('photos/general.comments') }}</th>
                        </tr>
                        <tr>
                            <th>{{ trans('photoComments/general.author') }}</th>
                            <th>{{ trans('photoComments/general.comment') }}</th>
                            <th>{{ trans('photoComments/general.createdAt') }}</th>
                        </tr>
                        @foreach ($photo->comments as $comment)
                            <tr>
                                <td>{{ $comment->user->name }}</td>
                                <td>{{ $comment->comment }}</td>
                                <td>{{ $comment->created_at }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="panel-body">
                    {!! Form::open(['method' => 'POST', 'url' => route('photos.comment.store')]) !!}
                        {!! Form::hidden('photo_id', $photo->id) !!}

                        <div class="form-group {{ $errors->has('comment') ? 'has-error' : ''}}">
                            {!! Form::label('comment', trans('photoComments/general.comment'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {{ Form::textarea('comment', null, ['class' => 'form-control md-input']) }}
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                {!! Form::submit(trans('button.save'), ['class' => 'col-sm-12 btn btn-success']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </section>
    </div>
@endsection
