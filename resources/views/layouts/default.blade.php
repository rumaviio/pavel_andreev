<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('partials.head')
        @yield('header_styles')
    </head>

    <body>
        <section id="container">
            @include('partials.header')

            @include('partials.sidebar')

            <div id="main-content">
                <section class="wrapper">
                    <!-- Notifications -->
                    @include('notifications')

                    @yield('content')
                </section>
            </div>
        </section>

        @include('partials.footer')
        @yield('footer_scripts')
    </body>
</html>
