<?php

return [

    'errors'    => 'Errors',
    'success'   => 'Success',
    'error'     => 'Error',
    'warning'   => 'Warning',
    'info'      => 'Info',
];
