<?php

return [

    'login'                 => 'Login',
    'email'                 => 'Email',
    'password'              => 'Password',
    'login'                 => 'Login',
    'registration'          => 'Don\'t have an account yet?',
    'createAccount'         => 'Create an account',
    'registration'          => 'Registration',
    'firstName'             => 'First name',
    'lastName'              => 'Last name',
    'username'              => 'Username',
    'email'                 => 'Email',
    'password'              => 'Password',
    'passwordConfirmation'  => 'Password Confirmation',
    'alreadyRegistered'     => 'Already Registered',

];
