<?php

return [

    'accountAlreadyExists' => 'An account with this email already exists.',
    'accountNotFound'      => 'Username or password is incorrect.',

    'successfullyLogout' => 'You have successfully logged out!',
    'loggedIn' => 'You must be logged in!',

    'login' => [
        'error'   => 'There was a problem while trying to log you in, please try again.',
        'success' => 'You have successfully logged in.',
    ],

];
