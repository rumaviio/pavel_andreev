<?php

return [

    'view'              => 'View',
    'info'              => 'Info',
    'edit'              => 'Edit',
    'delete'            => 'Delete',
    'save'              => 'Save',
    'submit'            => 'Submit',
    'cancel'            => 'Cancel',
    'create'            => 'Create',
    'back'              => 'Back',

];
