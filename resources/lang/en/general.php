<?php

return [

    'id'            => 'ID',
    'actions'       => 'Actions',
    'pleaseChoose'  => 'Please choose',
    'total'         => 'Total',
    'date'          => 'Date',

];
