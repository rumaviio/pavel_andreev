<?php

return [

    'logout'                => 'Logout',
    'profile'               => 'Profile',
    'firstName'             => 'First name',
    'lastName'              => 'Last name',
    'username'              => 'Username',
    'email'                 => 'Email',
    'password'              => 'Password',
    'passwordConfirmation'  => 'Password Confirmation',

];
