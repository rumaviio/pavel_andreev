<?php

return [

    'success' => [
        'create'    => 'Item was successfully created.',
        'update'    => 'Item was successfully updated.',
        'delete'    => 'Item was successfully deleted.',
        'duplicate' => 'Item was successfully duplicated.',
    ],

    'error' => [
        'create'        => 'There was an issue creating the Item. Please try again.',
        'update'        => 'There was an issue updating the Item. Please try again.',
        'delete'        => 'There was an issue deleting the Item. Please try again.',
        'itemExists'    => 'An Item already exists with that name, names must be unique for Items.',
    ],

];
