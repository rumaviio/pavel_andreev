<?php

return [

    'destroy' => [
        'title'     => 'Delete item',
        'body'      => 'Are you sure to delete this record? This operation is irreversible.',
        'cancel'    => 'Cancel',
        'confirm'   => 'Delete',
    ],

];
