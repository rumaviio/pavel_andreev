<?php

return [

    'name'          => 'Name',
    'image'         => 'Image',
    'catalogs'      => 'Catalogs',
    'comments'      => 'Comments',
    'author'        => 'Author',

];
